package net.uweeisele.springcloudstreams.cloudevents;

import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.internals.RecordHeaders;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.EmbeddedKafkaKraftBroker;
import org.springframework.kafka.test.utils.KafkaTestUtils;

import java.time.Duration;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class SpringCloudStreamsApplicationTests {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpringCloudStreamsApplicationTests.class);

	private static final String INPUT_TOPIC = "topic.source";
	private static final String OUTPUT_TOPIC = "topic.dest";
	private static final String GROUP_NAME = "filterTestGroup";

	public static EmbeddedKafkaBroker embeddedKafka = new EmbeddedKafkaKraftBroker(1, 2, INPUT_TOPIC, OUTPUT_TOPIC);

	@BeforeAll
	public static void setup() {
		embeddedKafka.afterPropertiesSet();
		System.setProperty("spring.cloud.stream.kafka.binder.brokers", embeddedKafka.getBrokersAsString());
	}

	@AfterAll
	public static void destroy() {
		embeddedKafka.destroy();
	}

	@Test
	void testSendReceive() throws InterruptedException, ExecutionException {
		Map<String, Object> senderProps = KafkaTestUtils.producerProps(embeddedKafka);
		senderProps.put("key.serializer", StringSerializer.class);
		senderProps.put("value.serializer", StringSerializer.class);
		try (KafkaProducer<String, String> producer = new KafkaProducer<>(senderProps)) {
			producer.send(new ProducerRecord<>(INPUT_TOPIC, null, null, "A", "HELLO",
					new RecordHeaders()
						.add("ce_specversion", "1.0".getBytes(UTF_8))
						.add("ce_type", "net.uweeisele.kafka.text".getBytes(UTF_8))
						.add("ce_source", "/test".getBytes(UTF_8))
						.add("ce_id", UUID.randomUUID().toString().getBytes(UTF_8))
						.add("ce_subject", "/text".getBytes(UTF_8))
						.add("content-type", "text/plain".getBytes(UTF_8)))).get();
			producer.send(new ProducerRecord<>(INPUT_TOPIC, null, null, "A", ":)",
					new RecordHeaders()
							.add("ce_specversion", "1.0".getBytes(UTF_8))
							.add("ce_type", "net.uweeisele.kafka.text".getBytes(UTF_8))
							.add("ce_source", "/test".getBytes(UTF_8))
							.add("ce_id", UUID.randomUUID().toString().getBytes(UTF_8))
							.add("ce_subject", "/chars".getBytes(UTF_8)))).get();
			producer.send(new ProducerRecord<>(INPUT_TOPIC, null, null, "A", "@",
					new RecordHeaders()
							.add("ce_specversion", "1.0".getBytes(UTF_8))
							.add("ce_type", "net.uweeisele.kafka.text".getBytes(UTF_8))
							.add("ce_source", "/test".getBytes(UTF_8))
							.add("ce_id", UUID.randomUUID().toString().getBytes(UTF_8))
							.add("ce_subject", "/chars".getBytes(UTF_8)))).get();
			producer.send(new ProducerRecord<>(INPUT_TOPIC, null, null, "A", "WORLD",
					new RecordHeaders()
							.add("ce_specversion", "1.0".getBytes(UTF_8))
							.add("ce_type", "net.uweeisele.kafka.text".getBytes(UTF_8))
							.add("ce_source", "/test".getBytes(UTF_8))
							.add("ce_id", UUID.randomUUID().toString().getBytes(UTF_8))
							.add("ce_subject", "/text".getBytes(UTF_8))
							.add("content-type", "text/plain".getBytes(UTF_8)))).get();
		}

		Map<String, Object> consumerProps = KafkaTestUtils.consumerProps(GROUP_NAME, "false", embeddedKafka);
		consumerProps.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
		consumerProps.put("key.deserializer", StringDeserializer.class);
		consumerProps.put("value.deserializer", StringDeserializer.class);
		Consumer<String, String> consumer = new KafkaConsumer<>(consumerProps);
		consumer.subscribe(Set.of(OUTPUT_TOPIC));
		ConsumerRecords<String, String> records = KafkaTestUtils.getRecords(consumer, Duration.ofSeconds(10), 2);
		consumer.commitSync();
		consumer.close();

		assertThat(records.count()).isEqualTo(2);
		Iterator<ConsumerRecord<String, String>> iterator = records.iterator();

		ConsumerRecord<String, String> record1 = iterator.next();
		System.out.println(record1);
		assertThat(record1.value()).isEqualTo("hello");
		assertThat(record1.key()).isEqualTo("A");
		assertThat(record1.headers().lastHeader("ce_specversion")).isNotNull();
		assertThat(new String(record1.headers().lastHeader("ce_specversion").value(), UTF_8)).isEqualTo("1.0");
		assertThat(record1.headers().lastHeader("ce_type")).isNotNull();
		assertThat(new String(record1.headers().lastHeader("ce_type").value(), UTF_8)).isEqualTo("net.uweeisele.kafka.text");
		assertThat(record1.headers().lastHeader("ce_source")).isNotNull();
		assertThat(new String(record1.headers().lastHeader("ce_source").value(), UTF_8)).isEqualTo("/test");
		assertThat(record1.headers().lastHeader("ce_id")).isNotNull();
		assertThat(new String(record1.headers().lastHeader("ce_id").value(), UTF_8)).isNotNull();
		assertThat(record1.headers().lastHeader("ce_subject")).isNotNull();
		assertThat(new String(record1.headers().lastHeader("ce_subject").value(), UTF_8)).isEqualTo("/text");
		assertThat(record1.headers().lastHeader("ce_partitionkey")).isNotNull();
		assertThat(new String(record1.headers().lastHeader("ce_partitionkey").value(), UTF_8)).isEqualTo("A");
		assertThat(record1.headers().lastHeader("content-type")).isNotNull();
		assertThat(new String(record1.headers().lastHeader("content-type").value(), UTF_8)).isEqualTo("text/plain");

		ConsumerRecord<String, String> record2 = iterator.next();
		assertThat(record2.value()).isEqualTo("world");
		assertThat(record2.headers().lastHeader("ce_subject")).isNotNull();
		assertThat(new String(record2.headers().lastHeader("ce_subject").value(), UTF_8)).isEqualTo("/text");
	}
}
