package net.uweeisele.springcloudstreams.cloudevents;

import org.springframework.cloud.function.cloudevent.CloudEventMessageUtils;

import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.Set;

import static java.nio.charset.StandardCharsets.UTF_8;

public class CloudEventsKafkaHeaderMapper extends PrefixKafkaHeaderMapper {

    private static final Charset CLOUD_EVENTS_CHARSET = UTF_8;

    private static final String CLOUD_EVENTS_PREFIX = CloudEventMessageUtils.KAFKA_ATTR_PREFIX;

    private static final Set<String> ADDITIONAL_HEADERS;
    static {
        ADDITIONAL_HEADERS = new HashSet<>();
        ADDITIONAL_HEADERS.add("content-type");
    }

    public CloudEventsKafkaHeaderMapper() {
        super(CLOUD_EVENTS_CHARSET, CLOUD_EVENTS_PREFIX, ADDITIONAL_HEADERS);
    }
}
