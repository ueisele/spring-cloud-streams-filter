package net.uweeisele.springcloudstreams.cloudevents;

import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.support.KafkaHeaderMapper;
import org.springframework.messaging.MessageHeaders;

import java.nio.charset.Charset;
import java.util.*;

public class PrefixKafkaHeaderMapper implements KafkaHeaderMapper {

    protected static final Logger LOGGER = LoggerFactory.getLogger(PrefixKafkaHeaderMapper.class);

    protected final Charset charset;

    protected final String prefix;

    protected final Set<String> additionalHeaderNames = new HashSet<>();

    public PrefixKafkaHeaderMapper(Charset charset, String prefix) {
        this(charset, prefix, null);
    }

    public PrefixKafkaHeaderMapper(Charset charset, String prefix, Set<String> additionalHeaderNames) {
        this.prefix = Objects.requireNonNull(prefix);
        Optional.ofNullable(additionalHeaderNames).ifPresent(this.additionalHeaderNames::addAll);
        this.charset = Objects.requireNonNull(charset);
    }

    @Override
    public void fromHeaders(MessageHeaders headers, Headers target) {
        headers.forEach((key, value) -> {
            if (key.startsWith(prefix) || additionalHeaderNames.contains(key)) {
                if (value instanceof String) {
                    target.add(new RecordHeader(key, ((String) value).getBytes(charset)));
                    LOGGER.debug("[{}] added to Kafka headers", key);
                }
            }
        });
    }

    @Override
    public void toHeaders(Headers source, Map<String, Object> target) {
        source.forEach(header -> {
            String headerName = header.key();
            if (headerName.startsWith(prefix) || additionalHeaderNames.contains(headerName)) {
                byte[] headerValue = header.value();
                if (headerValue != null && headerValue.length > 0) {
                    target.put(headerName, new String(header.value(), charset));
                    LOGGER.debug("[{}] added to Spring headers", headerName);
                }
            }
        });
    }
}
