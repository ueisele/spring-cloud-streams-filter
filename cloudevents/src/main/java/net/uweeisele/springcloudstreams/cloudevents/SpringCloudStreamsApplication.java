package net.uweeisele.springcloudstreams.cloudevents;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.function.cloudevent.CloudEventMessageBuilder;
import org.springframework.cloud.function.cloudevent.CloudEventMessageUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.expression.Expression;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.kafka.support.KafkaHeaderMapper;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.logging.Level;

import static java.nio.charset.StandardCharsets.UTF_8;

// see
// * https://docs.spring.io/spring-cloud-stream/docs/4.0.0-M4/reference/html/
// * https://spring.io/blog/2019/10/17/spring-cloud-stream-functional-and-reactive
@SpringBootApplication
public class SpringCloudStreamsApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpringCloudStreamsApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudStreamsApplication.class, args);
	}

	@Component
	@ConfigurationProperties("app")
	public static class AppProperties {
		private Expression sourceKeyExpression = new SpelExpressionParser().parseExpression("headers['ce-partitionkey']");

		public Expression getSourceKeyExpression() {
			return sourceKeyExpression;
		}

		public void setSourceKeyExpression(Expression sourceKeyExpression) {
			this.sourceKeyExpression = sourceKeyExpression;
		}
	}

	/**
	 * This method provides a function which filters messages based on the "subject" CloudEvents header.
	 * Depending on the CloudEvents binding, different prefixes for the attributes are used.
	 * For example, if you use Kafka, all headers are prefixed with "ce_". If AMQP is used, headers are prefixed with "cloudEvents:".
	 * In order to allow a protocol independent implementation, Spring Cloud normalizes CloudEvents headers.
	 * The normalization replaces the protocol-specific prefixes with "ce-".
	 * Therefore, in order to access CloudEvents headers in Spring Cloud, you must prefix them with "ce-".
	 * The normalization is done by a FunctionInvocationHelper, which is registered by CloudEventsFunctionExtensionConfiguration.
	 */
	@Bean
	public Function<Flux<Message<byte[]>>, Flux<Message<byte[]>>> filter() {
		return flux -> flux
				.filter(filterOrLog(msg -> CloudEventMessageUtils.isCloudEvent(msg) && "/text".equals(CloudEventMessageUtils.getSubject(msg)), LOGGER::debug));
	}

	@Bean
	public Function<Flux<Message<byte[]>>, Flux<Message<byte[]>>> processCloudEvent(AppProperties appProperties) {
		return flux -> flux
				.map(wrapCloudEvent(appProperties.getSourceKeyExpression()::getValue, Function.<byte[]>identity()
						.andThen(deserialize())
						.andThen(transform())
						.andThen(serialize())))
				.log(SpringCloudStreamsApplication.class.getName(), Level.FINE);
	}

	/**
	 * Custom KafkaHeaderMapper implementation, which deserializes all headers which start with ce_ prefix or content-type.
	 * This is important to allow protocol-independent implementations. For example, if AMQP is used, all headers are Strings by default.
	 */
	@Bean
	public KafkaHeaderMapper kafkaBinderHeaderMapper() {
		return new CloudEventsKafkaHeaderMapper();
	}

	private <T> Predicate<T> filterOrLog(Predicate<T> filter, BiConsumer<String, T> logger) {
		return item -> {
			if (!filter.test(item)) {
				logger.accept("Not matched filter criteria: {}", item);
				return false;
			} else {
				return true;
			}
		};
	}

	private <T, R> Function<Message<T>, Message<R>> wrapCloudEvent(Function<Message<T>, Object> keyMapper, Function<T, R> payloadMapper) {
		return msg -> CloudEventMessageBuilder
				.withData(payloadMapper.apply(msg.getPayload()))
				.copyHeaders(msg.getHeaders())
				.setHeader(CloudEventMessageUtils.DEFAULT_ATTR_PREFIX + "partitionkey", keyMapper.apply(msg)).build();
	}

	private Function<byte[], String> deserialize() {
		return String::new;
	}

	private Function<String, String> transform() {
		return String::toLowerCase;
	}

	private Function<String, byte[]> serialize() {
		return msg -> msg.getBytes(UTF_8);
	}

}