package net.uweeisele.springcloudstreams.kstreams;

import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.internals.RecordHeaders;
import org.apache.kafka.common.serialization.IntegerDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.common.utils.Utils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.EmbeddedKafkaKraftBroker;
import org.springframework.kafka.test.utils.KafkaTestUtils;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * ToDo
 * * Test to check the order is kept
 * * Test to check that streams processed in parallel and do not block each other
 * * Test to check that messages only committed if successfully published
 */
@SpringBootTest
class SpringCloudStreamsApplicationTests {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpringCloudStreamsApplicationTests.class);

	private static final String INPUT_TOPIC = "topic.source";
	private static final String OUTPUT_TOPIC = "topic.dest";
	private static final String GROUP_NAME = "filterTestGroup";

	public static EmbeddedKafkaBroker embeddedKafka = new EmbeddedKafkaKraftBroker(1, 2, INPUT_TOPIC, OUTPUT_TOPIC);

	@BeforeAll
	public static void setup() {
		embeddedKafka.afterPropertiesSet();
		LOGGER.debug("Embedded Kafka connection string: " + embeddedKafka.getBrokersAsString());
		System.setProperty("spring.cloud.stream.kafka.binder.brokers", embeddedKafka.getBrokersAsString());

		System.setProperty("app.parallelism", String.valueOf(4));
	}

	@AfterAll
	public static void destroy() {
		embeddedKafka.destroy();
	}

	@Test
	void testSendReceive() throws InterruptedException {
		String senderKey0 = keyForPartition(new KeySpec(0, 2));
		String senderKey1 = keyForPartition(new KeySpec(1, 2));

		Map<String, Object> senderProps = KafkaTestUtils.producerProps(embeddedKafka);
		senderProps.put("key.serializer", StringSerializer.class);
		senderProps.put("value.serializer", StringSerializer.class);

		AtomicBoolean shouldStop = new AtomicBoolean(false);
		Thread sender1 = new Thread(() -> sendRecords(senderProps, senderKey1, true, shouldStop));
		Thread sender2 = new Thread(() -> sendRecords(senderProps, senderKey1, false, shouldStop));
		sender1.start();
		sender2.start();

		Thread.sleep(5000);
		int numRecords = 10;
		Thread numSender1 = new Thread(() -> sendRecords(senderProps, senderKey0, numRecords));
		numSender1.start();
		numSender1.join();

		Thread.sleep(10000);
		shouldStop.set(true);
		sender1.join();
		sender2.join();
		/*
		try (KafkaProducer<String, String> producer = new KafkaProducer<>(senderProps)) {
			CountDownLatch latch0T = new CountDownLatch(numRecords);
			CountDownLatch latch0F = new CountDownLatch(numRecords);
			CountDownLatch latch1T = new CountDownLatch(numRecords);
			CountDownLatch latch1F = new CountDownLatch(numRecords);
			for (int i = 0; i < numRecords; i++) {
				producer.send(new ProducerRecord<>(INPUT_TOPIC, null, null, keyForPartition(0, 2), String.valueOf(i), new RecordHeaders().add("number", "true".getBytes(UTF_8))), (metadata, exception) -> {
					latch0T.countDown();
				});
				producer.send(new ProducerRecord<>(INPUT_TOPIC, null, null, keyForPartition(0, 2), "HELLO WORLD", new RecordHeaders().add("number", "false".getBytes(UTF_8))), (metadata, exception) -> {
					latch0F.countDown();
				});
				producer.send(new ProducerRecord<>(INPUT_TOPIC, null, null, keyForPartition(1, 2), String.valueOf(i), new RecordHeaders().add("number", "true".getBytes(UTF_8))), (metadata, exception) -> {
					latch1T.countDown();
				});
				producer.send(new ProducerRecord<>(INPUT_TOPIC, null, null, keyForPartition(1, 2), "HELLO WORLD", new RecordHeaders().add("number", "false".getBytes(UTF_8))), (metadata, exception) -> {
					latch1F.countDown();
				});
				if (i%5==0) {
					Thread.sleep(2000);
				}
			}
			latch0T.await();
			latch0F.await();
			latch1T.await();
			latch1F.await();
		}
		*/

		Map<String, Object> consumerProps = KafkaTestUtils.consumerProps(GROUP_NAME, "false", embeddedKafka);
		consumerProps.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
		consumerProps.put("key.deserializer", StringDeserializer.class);
		consumerProps.put("value.deserializer", IntegerDeserializer.class);
		Consumer<String, Integer> consumer = new KafkaConsumer<>(consumerProps);
		consumer.subscribe(Set.of(OUTPUT_TOPIC));
		HashMap<String, AtomicInteger> expectedValues = new HashMap<>();
		ConsumerRecords<String, Integer> records;
		do {
			records = KafkaTestUtils.getRecords(consumer, Duration.ofSeconds(8), numRecords);
			for (ConsumerRecord<String, Integer> record : records) {
				AtomicInteger expectedValue = expectedValues.computeIfAbsent(record.key(), key -> new AtomicInteger(0));
				assertThat(record.value()).isEqualTo(expectedValue.get());
				expectedValue.incrementAndGet();
			}
		} while (!records.isEmpty());
		consumer.commitSync();
		consumer.close();
	}

	private void sendRecords(Map<String, Object> senderProps, String key, int numRecords) {
		try (KafkaProducer<String, String> producer = new KafkaProducer<>(senderProps)) {
			CountDownLatch latch = new CountDownLatch(numRecords);
			for (int i = 0; i < numRecords; i++) {
				producer.send(new ProducerRecord<>(INPUT_TOPIC, null, null, key, String.valueOf(i), new RecordHeaders().add("number", "true".getBytes(UTF_8))), (metadata, exception) -> {
					latch.countDown();
				});
			}
			latch.await();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private void sendRecords(Map<String, Object> senderProps, String key, boolean number, AtomicBoolean shouldStop) {
		try (KafkaProducer<String, String> producer = new KafkaProducer<>(senderProps)) {
			long i = 0;
			while (!shouldStop.get()){
				producer.send(new ProducerRecord<>(INPUT_TOPIC, null, null, key, number ? String.valueOf(i) : "A" + i, new RecordHeaders().add("number", String.valueOf(number).getBytes(UTF_8)))).get();
				Thread.sleep(500);
				i++;
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private String keyForPartition(KeySpec... keySpecs) {
		AtomicReference<String> key = new AtomicReference<>();
		do {
			key.set(UUID.randomUUID().toString());
		} while(!Arrays.stream(keySpecs).allMatch(keySpec -> Utils.toPositive(Utils.murmur2(key.get().getBytes(UTF_8))) % keySpec.numPartitions == keySpec.partition));
		return key.get();
	}

	private record KeySpec(int partition, int numPartitions) {}
}
