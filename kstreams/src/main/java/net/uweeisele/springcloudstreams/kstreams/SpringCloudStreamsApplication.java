package net.uweeisele.springcloudstreams.kstreams;

import org.apache.kafka.common.header.Headers;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.ValueMapperWithKey;
import org.apache.kafka.streams.kstream.ValueTransformer;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

import java.util.Collections;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

import static java.nio.charset.StandardCharsets.UTF_8;

// see
// * https://docs.spring.io/spring-cloud-stream/docs/4.0.0-M4/reference/html/
@EnableConfigurationProperties
@SpringBootApplication
public class SpringCloudStreamsApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpringCloudStreamsApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudStreamsApplication.class, args);
	}

	@Bean
	public Function<KStream<String, String>, KStream<String, String>> filter() {
		return kStream -> kStream.flatTransformValues(() -> new HeaderFilter<>(headers -> new String(headers.lastHeader("number").value(), UTF_8).equals("true")));
	}

	@Bean
	public Function<KStream<String, String>, KStream<String, Integer>> processor() {
		return kStream -> kStream
				.mapValues(transform())
				.transformValues(this::longRunningAction)
				.peek((key, value) -> LOGGER.debug("## Processed value {} -> {}", key, value));
	}

	private ValueMapperWithKey<String, String, Integer> transform() {
		return (readOnlyKey, value) -> Integer.parseInt(value);
	}

	private ValueContextTransformer<Integer, Integer> longRunningAction() {
		return new ValueContextTransformer<>((context, value) -> {
			int processDuration = context.partition() % 2 == 0 ? 4000 : 1;
			LOGGER.debug("## Process for {} at offset {}/{} ms -> {}", processDuration, context.partition(), context.offset(), value);
			try {
				Thread.sleep(processDuration);
			} catch (InterruptedException e) {
				LOGGER.debug("## Processing interrupted at offset {}/{} -> {}", context.partition(), context.offset(), value);
			}
			return value;
		});
	}

	private static class HeaderFilter<V> implements ValueTransformer<V, Iterable<V>> {

		private final Predicate<Headers> filter;

		private ProcessorContext context;

		public HeaderFilter(Predicate<Headers> filter) {
			this.filter = filter;
		}

		@Override
		public void init(ProcessorContext context) {
			this.context = context;
		}

		@Override
		public Iterable<V> transform(V value) {
			if (filter.test(context.headers())) {
				return Collections.singletonList(value);
			} else {
				return Collections.emptyList();
			}
		}

		@Override
		public void close() {}
	}

	private static class ValueContextTransformer<V, VR> implements ValueTransformer<V, VR> {

		private final BiFunction<ProcessorContext, V, VR> transformer;

		private ProcessorContext context;

		public ValueContextTransformer(BiFunction<ProcessorContext, V, VR> transformer) {
			this.transformer = transformer;
		}

		@Override
		public void init(ProcessorContext context) {
			this.context = context;
		}

		@Override
		public VR transform(V value) {
			return transformer.apply(context, value);
		}

		@Override
		public void close() {}
	}
}