package net.uweeisele.springcloudstreams.reactive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.support.KafkaHeaderMapper;
import org.springframework.kafka.support.SimpleKafkaHeaderMapper;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;
import reactor.core.publisher.Flux;

import java.util.Objects;
import java.util.function.Function;
import java.util.logging.Level;

import static java.nio.charset.StandardCharsets.UTF_8;

// see
// * https://docs.spring.io/spring-cloud-stream/docs/4.0.0-M4/reference/html/
// * https://spring.io/blog/2019/10/17/spring-cloud-stream-functional-and-reactive
@SpringBootApplication
public class SpringCloudStreamsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudStreamsApplication.class, args);
	}

	@Bean
	public Function<Flux<Message<byte[]>>, Flux<Message<byte[]>>> filter() {
		return flux -> flux.filter(message -> Objects.equals(message.getHeaders().get("subject", String.class), "/text"));
	}

	@Bean
	public Function<Flux<Message<byte[]>>, Flux<Message<String>>> deserialize() {
		return flux -> flux.map(bytesMsg -> new GenericMessage<>(new String(bytesMsg.getPayload(), UTF_8), bytesMsg.getHeaders()));
	}

	@Bean
	public Function<Flux<Message<String>>, Flux<Message<String>>> process() {
		return flux -> flux.map(stringMsg -> (Message<String>)new GenericMessage<>(stringMsg.getPayload().toLowerCase(), stringMsg.getHeaders()))
				.log(SpringCloudStreamsApplication.class.getName(), Level.FINE);
	}

	@Bean
	public Function<Flux<Message<String>>, Flux<Message<byte[]>>> serialize() {
		return flux -> flux.map(stringMsg -> new GenericMessage<>(stringMsg.getPayload().getBytes(UTF_8), stringMsg.getHeaders()));
	}

	/**
	 * Kafka header values are just bytes. By default, Kafka headers are not deserialized.
	 * As consequence e.g. filtering based on header values does not work with Kafka.
	 * In order to deserialize Kafka header values as String, a KafkaHeaderMapper bean with the name kafkaBinderHeaderMapper must be provided.
	 * If you just want to deserialize some specific headers, the SimpleKafkaHeaderMapper can be used.
	 */
	@Bean
	public KafkaHeaderMapper kafkaBinderHeaderMapper() {
		SimpleKafkaHeaderMapper kafkaHeaderMapper = new SimpleKafkaHeaderMapper();
		kafkaHeaderMapper.addRawMappedHeader("subject", true);
		return kafkaHeaderMapper;
	}

}