package net.uweeisele.springcloudstreams.reactive;

import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.internals.RecordHeaders;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.EmbeddedKafkaKraftBroker;
import org.springframework.kafka.test.utils.KafkaTestUtils;

import java.time.Duration;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class SpringCloudStreamsApplicationTests {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpringCloudStreamsApplicationTests.class);

	private static final String INPUT_TOPIC = "topic.source";
	private static final String OUTPUT_TOPIC = "topic.dest";
	private static final String GROUP_NAME = "filterTestGroup";

	public static EmbeddedKafkaBroker embeddedKafka = new EmbeddedKafkaKraftBroker(1, 1, INPUT_TOPIC, OUTPUT_TOPIC);

	@BeforeAll
	public static void setup() {
		embeddedKafka.afterPropertiesSet();
		System.setProperty("spring.cloud.stream.kafka.binder.brokers", embeddedKafka.getBrokersAsString());
	}

	@AfterAll
	public static void destroy() {
		embeddedKafka.destroy();
	}

	@Test
	void testSendReceive() throws InterruptedException, ExecutionException {
		Map<String, Object> senderProps = KafkaTestUtils.producerProps(embeddedKafka);
		senderProps.put("value.serializer", StringSerializer.class);
		try (KafkaProducer<byte[], String> producer = new KafkaProducer<>(senderProps)) {
			producer.send(new ProducerRecord<>(INPUT_TOPIC, null, null, null, "HELLO", new RecordHeaders().add("subject", "/text".getBytes(UTF_8)))).get();
			producer.send(new ProducerRecord<>(INPUT_TOPIC, null, null, null, ":)", new RecordHeaders().add("subject", "/chars".getBytes(UTF_8)))).get();
			producer.send(new ProducerRecord<>(INPUT_TOPIC, null, null, null, "@", new RecordHeaders().add("subject", "/chars".getBytes(UTF_8)))).get();
			producer.send(new ProducerRecord<>(INPUT_TOPIC, null, null, null, "WORLD", new RecordHeaders().add("subject", "/text".getBytes(UTF_8)))).get();
		}

		Map<String, Object> consumerProps = KafkaTestUtils.consumerProps(GROUP_NAME, "false", embeddedKafka);
		consumerProps.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
		consumerProps.put("value.deserializer", StringDeserializer.class);
		Consumer<byte[], String> consumer = new KafkaConsumer<>(consumerProps);
		consumer.subscribe(Set.of(OUTPUT_TOPIC));
		ConsumerRecords<byte[], String> records = KafkaTestUtils.getRecords(consumer, Duration.ofSeconds(10), 2);
		consumer.commitSync();
		consumer.close();

		assertThat(records.count()).isEqualTo(2);
		Iterator<ConsumerRecord<byte[], String>> iterator = records.iterator();

		ConsumerRecord<byte[], String> record1 = iterator.next();
		assertThat(record1.value()).isEqualTo("hello");
		assertThat(record1.headers().lastHeader("subject")).isNotNull();
		assertThat(new String(record1.headers().lastHeader("subject").value(), UTF_8)).isEqualTo("/text");

		ConsumerRecord<byte[], String> record2 = iterator.next();
		assertThat(record2.value()).isEqualTo("world");
		assertThat(record2.headers().lastHeader("subject")).isNotNull();
		assertThat(new String(record2.headers().lastHeader("subject").value(), UTF_8)).isEqualTo("/text");
	}
}
