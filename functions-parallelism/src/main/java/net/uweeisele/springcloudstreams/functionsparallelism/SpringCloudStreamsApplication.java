package net.uweeisele.springcloudstreams.functionsparallelism;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.support.KafkaHeaderMapper;
import org.springframework.kafka.support.SimpleKafkaHeaderMapper;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;

import java.nio.ByteBuffer;
import java.util.Objects;
import java.util.function.Function;

import static java.nio.charset.StandardCharsets.UTF_8;

// see
// * https://docs.spring.io/spring-cloud-stream/docs/4.0.0-M4/reference/html/
@SpringBootApplication
public class SpringCloudStreamsApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpringCloudStreamsApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudStreamsApplication.class, args);
	}

	@Bean
	public Function<Message<byte[]>, Message<byte[]>> filter() {
		return msg -> {
			LOGGER.debug("filter -> {}", msg);
			return Objects.equals(msg.getHeaders().get("number", String.class), "true") ? msg : null;
		};
	}

	@Bean
	public Function<Message<byte[]>, Message<String>> deserialize() {
		return bytesMsg -> new GenericMessage<>(new String(bytesMsg.getPayload(), UTF_8), bytesMsg.getHeaders());
	}

	@Bean
	public Function<Message<String>, Message<Integer>> process() {
		return stringMsg -> {
			LOGGER.debug("process -> {}", stringMsg);
			return new GenericMessage<>(Integer.valueOf(stringMsg.getPayload()), stringMsg.getHeaders());
		};
	}

	@Bean
	public Function<Message<Integer>, Message<byte[]>> serialize() {
		return intMsg -> new GenericMessage<>(ByteBuffer.allocate(4).putInt(intMsg.getPayload()).array(), intMsg.getHeaders());
	}

	/**
	 * Kafka header values are just bytes. By default, Kafka headers are not deserialized.
	 * As consequence e.g. filtering based on header values does not work with Kafka.
	 * In order to deserialize Kafka header values as String, a KafkaHeaderMapper bean with the name kafkaBinderHeaderMapper must be provided.
	 * If you just want to deserialize some specific headers, the SimpleKafkaHeaderMapper can be used.
	 */
	@Bean
	public KafkaHeaderMapper kafkaBinderHeaderMapper() {
		SimpleKafkaHeaderMapper kafkaHeaderMapper = new SimpleKafkaHeaderMapper();
		kafkaHeaderMapper.addRawMappedHeader("number", true);
		return kafkaHeaderMapper;
	}

}