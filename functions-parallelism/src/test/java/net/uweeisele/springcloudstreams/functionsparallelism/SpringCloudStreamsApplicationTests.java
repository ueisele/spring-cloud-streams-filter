package net.uweeisele.springcloudstreams.functionsparallelism;

import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.internals.RecordHeaders;
import org.apache.kafka.common.serialization.IntegerDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.common.utils.Utils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.EmbeddedKafkaKraftBroker;
import org.springframework.kafka.test.utils.KafkaTestUtils;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;

@Disabled
@SpringBootTest
class SpringCloudStreamsApplicationTests {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpringCloudStreamsApplicationTests.class);

	private static final String INPUT_TOPIC = "topic.source";
	private static final String OUTPUT_TOPIC = "topic.dest";
	private static final String GROUP_NAME = "filterTestGroup";

	public static EmbeddedKafkaBroker embeddedKafka = new EmbeddedKafkaKraftBroker(1, 2, INPUT_TOPIC, OUTPUT_TOPIC);

	@BeforeAll
	public static void setup() {
		embeddedKafka.afterPropertiesSet();
		System.setProperty("spring.cloud.stream.kafka.binder.brokers", embeddedKafka.getBrokersAsString());
	}

	@AfterAll
	public static void destroy() {
		embeddedKafka.destroy();
	}

	@Test
	void testSendReceive() throws InterruptedException, ExecutionException {
		Map<String, Object> senderProps = KafkaTestUtils.producerProps(embeddedKafka);
		senderProps.put("key.serializer", StringSerializer.class);
		senderProps.put("value.serializer", StringSerializer.class);
		int numRecords = 1000;
		try (KafkaProducer<String, String> producer = new KafkaProducer<>(senderProps)) {
			CountDownLatch latch0T = new CountDownLatch(numRecords);
			CountDownLatch latch0F = new CountDownLatch(numRecords);
			CountDownLatch latch1T = new CountDownLatch(numRecords);
			CountDownLatch latch1F = new CountDownLatch(numRecords);
			for (int i = 0; i < numRecords; i++) {
				producer.send(new ProducerRecord<>(INPUT_TOPIC, null, null, keyForPartition(0, 2), String.valueOf(i), new RecordHeaders().add("number", "true".getBytes(UTF_8))), (metadata, exception) -> {
					latch0T.countDown();
				});
				producer.send(new ProducerRecord<>(INPUT_TOPIC, null, null, keyForPartition(0, 2), "HELLO WORLD", new RecordHeaders().add("number", "false".getBytes(UTF_8))), (metadata, exception) -> {
					latch0F.countDown();
				});
				producer.send(new ProducerRecord<>(INPUT_TOPIC, null, null, keyForPartition(1, 2), String.valueOf(i), new RecordHeaders().add("number", "true".getBytes(UTF_8))), (metadata, exception) -> {
					latch1T.countDown();
				});
				producer.send(new ProducerRecord<>(INPUT_TOPIC, null, null, keyForPartition(1, 2), "HELLO WORLD", new RecordHeaders().add("number", "false".getBytes(UTF_8))), (metadata, exception) -> {
					latch1F.countDown();
				});
			}
			latch0T.await();
			latch0F.await();
			latch1T.await();
			latch1F.await();
		}

		Map<String, Object> consumerProps = KafkaTestUtils.consumerProps(GROUP_NAME, "false", embeddedKafka);
		consumerProps.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
		consumerProps.put("key.deserializer", IntegerDeserializer.class);
		consumerProps.put("value.deserializer", IntegerDeserializer.class);
		Consumer<String, Integer> consumer = new KafkaConsumer<>(consumerProps);
		consumer.subscribe(Set.of(OUTPUT_TOPIC));
		ConsumerRecords<String, Integer> records = KafkaTestUtils.getRecords(consumer, Duration.ofSeconds(30), numRecords * 2);
		consumer.commitSync();
		consumer.close();

		assertThat(records.count()).isEqualTo(numRecords * 2);
		HashMap<Integer, AtomicInteger> expectedValues = new HashMap<>();
		expectedValues.put(0, new AtomicInteger(0));
		expectedValues.put(1, new AtomicInteger(0));
		for (ConsumerRecord<String, Integer> record : records) {
			AtomicInteger expectedValue = expectedValues.get(record.partition());
			LOGGER.info("Partition {} -> Expected value: {}; Actual value: {}", record.partition(), expectedValue.get(), record.value());
			assertThat(record.value()).isEqualTo(expectedValue.get());
			expectedValue.incrementAndGet();
		}
	}

	private String keyForPartition(int partition, int numPartitions) {
		String key;
		do {
			key = UUID.randomUUID().toString();
		} while(Utils.toPositive(Utils.murmur2(key.getBytes(UTF_8))) % numPartitions != partition);
		return key;
	}
}
