package net.uweeisele.springcloudstreams.parallelism;

import org.apache.kafka.common.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.support.KafkaHeaderMapper;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.kafka.support.SimpleKafkaHeaderMapper;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import java.nio.ByteBuffer;
import java.time.Duration;
import java.util.Objects;
import java.util.Random;
import java.util.function.Function;
import java.util.logging.Level;

import static java.util.Objects.requireNonNullElse;

// see
// * https://docs.spring.io/spring-cloud-stream/docs/4.0.0-M4/reference/html/
// * https://spring.io/blog/2019/10/17/spring-cloud-stream-functional-and-reactive
@EnableConfigurationProperties
@SpringBootApplication
public class SpringCloudStreamsApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpringCloudStreamsApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudStreamsApplication.class, args);
	}

	public record AppProperties(int parallelism) {}

	@Bean
	@ConfigurationProperties("app")
	public AppProperties appProperties(@Value("${app.parallelism:4}") int parallelism) {
		return new AppProperties(parallelism);
	}

	@Bean(destroyMethod = "dispose")
	public Scheduler scheduler(AppProperties appProperties) {
		return Schedulers.newParallel("processor", appProperties.parallelism());
	}

	@Bean
	public Function<Flux<Message<byte[]>>, Flux<Message<byte[]>>> filter() {
		return flux -> flux.filter(msg -> Objects.equals(msg.getHeaders().get("number", String.class), "true"));
				//.log(SpringCloudStreamsApplication.class.getName(), Level.FINE);
	}

	@Bean
	public Function<Flux<Message<byte[]>>, Flux<Message<byte[]>>> reactiveProcessor(AppProperties appProperties, Scheduler scheduler) {
		return flux -> flux
				.groupBy(msg -> partition(msg.getHeaders().get(KafkaHeaders.RECEIVED_KEY, byte[].class), appProperties.parallelism()))
				.parallel(appProperties.parallelism()).runOn(scheduler)
				.flatMap(g -> g.map(Function.<Message<byte[]>>identity()
						.andThen(deserialize())
						.andThen(transform())
						.andThen(longRunningAction())
						.andThen(serialize())))
				.sequential()
				.log(SpringCloudStreamsApplication.class.getName(), Level.FINE);
				//.map(msg -> {
				//	msg.getHeaders().get(KafkaHeaders.ACKNOWLEDGMENT, Acknowledgment.class).acknowledge();
				//	return msg;
				//});
	}

	@Bean
	public Function<Flux<Message<byte[]>>, Flux<Message<byte[]>>> reactiveProcessorMapSequential() {
		return flux -> flux
				.map(deserialize())
				.map(transform())
				.flatMapSequential(this::longRunningActionMapSequential, 4)
				.map(serialize())
				.log(SpringCloudStreamsApplication.class.getName(), Level.FINE);
				//.map(msg -> {
				//	msg.getHeaders().get(KafkaHeaders.ACKNOWLEDGMENT, Acknowledgment.class).acknowledge();
				//	return msg;
				//});
	}

	private int partition(byte[] key, int numPartitions) {
		return Utils.toPositive(Utils.murmur2(key)) % numPartitions;
	}

	@Bean
	public Function<Message<byte[]>, Message<String>> deserialize() {
		return bytesMsg -> MessageBuilder
				.withPayload(new String(bytesMsg.getPayload()))
				.copyHeaders(bytesMsg.getHeaders())
				.setHeader(KafkaHeaders.KEY, bytesMsg.getHeaders().get(KafkaHeaders.RECEIVED_KEY, byte[].class)).build();
	}

	@Bean
	public Function<Message<String>, Message<Integer>> transform() {
		return msg -> MessageBuilder
				.withPayload(Integer.valueOf(msg.getPayload()))
				.copyHeaders(msg.getHeaders()).build();
	}

	@Bean
	public Function<Message<Integer>, Message<Integer>> longRunningAction() {
		return msg -> {
			int processDuration = requireNonNullElse(msg.getHeaders().get(KafkaHeaders.RECEIVED_PARTITION, Integer.class), 0) % 2 == 0 ? 4000 : 1;
			//int processDuration = Integer.parseInt(requireNonNullElse(msg.getHeaders().get("processms", String.class), "0"));
			LOGGER.debug("## Process for {} ms -> {}", processDuration, msg);
			try {
				Thread.sleep(processDuration);
			} catch (InterruptedException e) {
				LOGGER.debug("## Processing interrupted -> {}", msg);
			}
			return msg;
		};
	}

	private Mono<Message<Integer>> longRunningActionMapSequential(Message<Integer> msg) {
		Random random = new Random();
		int processDuration = requireNonNullElse(msg.getHeaders().get(KafkaHeaders.RECEIVED_PARTITION, Integer.class), 0) % 2 == 0 ?
				random.nextInt(1000, 3000) : random.nextInt(1, 1000);
		//int processDuration = Integer.parseInt(requireNonNullElse(msg.getHeaders().get("processms", String.class), "0"));
		LOGGER.debug("## Process for {} ms -> {}", processDuration, msg);
		return Mono.just(msg).delayElement(Duration.ofMillis(processDuration), Schedulers.parallel());
	}

	@Bean
	public Function<Message<Integer>, Message<byte[]>> serialize() {
		return intMsg -> MessageBuilder
				.withPayload(ByteBuffer.allocate(4).putInt(intMsg.getPayload()).array())
				.copyHeaders(intMsg.getHeaders()).build();
	}

	/**
	 * Kafka header values are just bytes. By default, Kafka headers are not deserialized.
	 * As consequence e.g. filtering based on header values does not work with Kafka.
	 * In order to deserialize Kafka header values as String, a KafkaHeaderMapper bean with the name kafkaBinderHeaderMapper must be provided.
	 * If you just want to deserialize some specific headers, the SimpleKafkaHeaderMapper can be used.
	 */
	@Bean
	public KafkaHeaderMapper kafkaBinderHeaderMapper() {
		SimpleKafkaHeaderMapper kafkaHeaderMapper = new SimpleKafkaHeaderMapper();
		kafkaHeaderMapper.addRawMappedHeader("number", true);
		kafkaHeaderMapper.addRawMappedHeader("processms", true);
		return kafkaHeaderMapper;
	}

}