package net.uweeisele.springcloudstreams.functions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.support.KafkaHeaderMapper;
import org.springframework.kafka.support.SimpleKafkaHeaderMapper;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;

import java.util.Objects;
import java.util.function.Function;

import static java.nio.charset.StandardCharsets.UTF_8;

// see
// * https://docs.spring.io/spring-cloud-stream/docs/4.0.0-M4/reference/html/
// The default function interface does not support filtering. It is not possible to:
// * return null-Value
// * return Message-Value with null-Payload
// * return an Optional-Value
// * return an iterable-Value
// This approach requires it to always return a Message-Value which is not null.
// Filtering with this approach is just not possible!
// The only option is to create a filter method, which publishes to a Kafka topic!
@SpringBootApplication
public class SpringCloudStreamsApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpringCloudStreamsApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudStreamsApplication.class, args);
	}

	@Bean
	public Function<Message<byte[]>, Message<byte[]>> filter() {
		return msg -> Objects.equals(msg.getHeaders().get("subject", String.class), "/text") ? msg : null;
	}

	@Bean
	public Function<Message<byte[]>, Message<String>> deserialize() {
		return bytesMsg -> {
			if (bytesMsg == null)
				return null;
			return new GenericMessage<>(new String(bytesMsg.getPayload(), UTF_8), bytesMsg.getHeaders());
		};
	}

	@Bean
	public Function<Message<String>, Message<String>> process() {
		return stringMsg -> {
			if (stringMsg == null) {
				return null;
			}
			LOGGER.debug("process -> {}", stringMsg);
			return new GenericMessage<>(stringMsg.getPayload().toLowerCase(), stringMsg.getHeaders());
		};
	}

	@Bean
	public Function<Message<String>, Message<byte[]>> serialize() {
		return stringMsg -> {
			if (stringMsg == null) {
				return null;
			}
			return new GenericMessage<>(stringMsg.getPayload().getBytes(UTF_8), stringMsg.getHeaders());
		};
	}

	/**
	 * Kafka header values are just bytes. By default, Kafka headers are not deserialized.
	 * As consequence e.g. filtering based on header values does not work with Kafka.
	 * In order to deserialize Kafka header values as String, a KafkaHeaderMapper bean with the name kafkaBinderHeaderMapper must be provided.
	 * If you just want to deserialize some specific headers, the SimpleKafkaHeaderMapper can be used.
	 */
	@Bean
	public KafkaHeaderMapper kafkaBinderHeaderMapper() {
		SimpleKafkaHeaderMapper kafkaHeaderMapper = new SimpleKafkaHeaderMapper();
		kafkaHeaderMapper.addRawMappedHeader("subject", true);
		return kafkaHeaderMapper;
	}

}